﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehavior_AnimationsScene : MonoBehaviour {
	public GameObject PlayerCamPosition;
	public GameObject MonsterCamPosition;
	public GameObject NPCCamPosition;

	private Vector3 targetPosition;
	private int positionIndex = 0;

	public void ChangePosition(int newIndex) { positionIndex = newIndex; }

	void Update() {
		switch(positionIndex) {
		case 0:
			targetPosition = PlayerCamPosition.transform.position;
			break;
		case 1:
			targetPosition = MonsterCamPosition.transform.position;
			break;
		case 2:
			targetPosition = NPCCamPosition.transform.position;
			break;
		default:
			positionIndex = 0;
			break;
		}

		this.gameObject.transform.position = Vector3.MoveTowards(this.gameObject.transform.position, targetPosition, 0.5f);
	}
}
