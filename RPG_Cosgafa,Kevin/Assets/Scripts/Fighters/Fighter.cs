﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Fighter : MonoBehaviour {
	public enum FSM_STATE { IDLE, MOVE, ATTACK, DEAD, SKILL }
	public FSM_STATE currentState { get; protected set; }

	public Stats stats = new Stats();
	public Image hpBar;

	protected NavMeshAgent agent;
	protected GameObject target;

	protected virtual void UpdateIdleState() { }
	protected virtual void UpdateMoveState() { }
	protected virtual void UpdateAttackState() { }
	protected virtual void UpdateDeadState() { }
	protected virtual void UpdateSkillState() { }

	protected virtual void AdditionalStart() { }
	void Start() {
		AdditionalStart();

		agent =  this.GetComponent<NavMeshAgent>();
		currentState = FSM_STATE.IDLE;
	}

	protected virtual void AdditionalUpdate() { }
	void Update() {
		AdditionalUpdate();

		hpBar.fillAmount = (float)stats.currentHealth / (float)stats.maxHealth;

		switch(currentState) {
		case FSM_STATE.IDLE:
			UpdateIdleState();
			break;
		case FSM_STATE.MOVE:
			UpdateMoveState();
			break;
		case FSM_STATE.ATTACK:
			UpdateAttackState();
			break;
		case FSM_STATE.DEAD:
			UpdateDeadState();
			if(this.gameObject.GetComponent<CapsuleCollider>().enabled == true) {
				this.gameObject.GetComponent<CapsuleCollider>().enabled = false;
				this.gameObject.GetComponent<NavMeshAgent>().enabled = false;
			}
			break;
		default:
			currentState = FSM_STATE.IDLE;
			break;
		}
	}

	void DamageTo() {
		if(target != null) { target.GetComponent<Fighter>().Hit(this.stats.attackPower, this.gameObject); }
	}

	public void Hit(int value, GameObject attacker) {
		stats.DamageHealth(value);
		if(stats.currentHealth <= 0) {
			currentState = FSM_STATE.DEAD;
			attacker.GetComponent<Fighter>().stats.AddExperience(this.stats.experience);
		}
	}
}
