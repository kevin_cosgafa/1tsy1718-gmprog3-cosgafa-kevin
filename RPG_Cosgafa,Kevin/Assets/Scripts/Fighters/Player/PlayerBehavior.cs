﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerBehavior : Fighter {
	public GameObject movementMarkerPrefab;
	public Text statDisplay;
	public Image experienceBar;
	public GameObject plusVitality;
	public GameObject plusStrength;
	public GameObject inventoryMenu;

	private Ray ray;
	private RaycastHit hit;
	private GameObject movementMarker;
	private float stuckTimer = 0.0f;

	protected override void UpdateIdleState() {
		if(target != null) { currentState = FSM_STATE.ATTACK; return; }
		if(movementMarker != null) { currentState = FSM_STATE.MOVE; return; }

		if(agent.velocity.magnitude <= 2.5) { this.GetComponent<PlayerAnimations>().IdleArmed(); }

		Controls();
	}

	protected override void UpdateMoveState() {
		if(movementMarker == null) { currentState = FSM_STATE.IDLE; return; }
		if(target != null) {
			if(Vector3.Distance(this.gameObject.transform.position, target.transform.position) < 2.0f) { currentState = FSM_STATE.ATTACK; return; }
			else { movementMarker.transform.position = target.transform.position; }
		}

		this.GetComponent<PlayerAnimations>().MoveArmed();

		agent.SetDestination(movementMarker.transform.position);

		if(Vector3.Distance(this.gameObject.transform.position, movementMarker.transform.position) < 1.0f) { Destroy(movementMarker); stuckTimer = 0.0f; }

		else if(agent.velocity.magnitude == 0.0f) {
			stuckTimer += Time.deltaTime;

			if(stuckTimer >= 1.0f) {
				Destroy(movementMarker);
				stuckTimer = 0.0f;
				agent.SetDestination(agent.transform.position);
			}
		}

		Controls();
	}

	protected override void UpdateAttackState() {
		if(target == null || movementMarker == null || target.GetComponent<MonsterBehavior>().currentState == FSM_STATE.DEAD) {
			target = null;
			Destroy(movementMarker);
			currentState = FSM_STATE.IDLE;
			return;
		}
		if(Vector3.Distance(this.gameObject.transform.position, target.transform.position) > 2.0f) { currentState = FSM_STATE.MOVE; return; }

		this.GetComponent<PlayerAnimations>().Attack();
		this.gameObject.transform.LookAt(target.transform.position);

		movementMarker.transform.position = target.transform.position;

		Controls();
	}

	protected override void UpdateDeadState() {
		this.GetComponent<PlayerAnimations>().Dead();

		if(target != null) { target = null; }
		if(movementMarker != null) { Destroy(movementMarker); }
		if(agent.enabled == true) { agent.destination = this.gameObject.transform.position; }
	}

	protected override void UpdateSkillState() {
		this.GetComponent<PlayerAnimations>().Skill();


	}

	private void Controls() {
		if(EventSystem.current.IsPointerOverGameObject ()) { return; }
		ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		if(Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit)) {
			target = null;

			if(movementMarker == null) {
				movementMarker = Instantiate(movementMarkerPrefab, this.gameObject.transform.position, Quaternion.identity) as GameObject;
			}

			if(hit.collider.gameObject.tag == "Monster") { target = hit.collider.gameObject; movementMarker.transform.position = target.transform.position; }

			else { movementMarker.transform.position = hit.point; }
		}
	}

	protected override void AdditionalStart() {
		stats.SetStrength(4);
		stats.SetVitality(1004);
	}

	protected override void AdditionalUpdate() {
		experienceBar.fillAmount = (float)stats.experience / (float)stats.experienceToLevel;
		statDisplay.text =
			" Koko\n " +
			stats.level + "\n " +
			stats.maxHealth + "\n " +
			stats.attackPower + "\n " +
			stats.vitality + "\n " +
			stats.strength + "\n " +
			stats.experience;

		if(stats.statPoints <= 0) { plusStrength.SetActive(false); plusVitality.SetActive(false); }

		if(stats.statPoints > 0) { plusStrength.SetActive(true); plusVitality.SetActive(true); }

		if(Input.GetKeyDown(KeyCode.I)) { inventoryMenu.SetActive(!inventoryMenu.activeSelf); }
	}

	public void AddStrength() { stats.IncrementStrength(); }
	public void AddVitality() { stats.IncrementVitality(); }

	public void UseSkill() {
		currentState = FSM_STATE.SKILL;
	}
}
