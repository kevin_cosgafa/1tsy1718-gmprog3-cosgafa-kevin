﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Skill {
	public string name;
	public Image uiImage;
	public float coolDown;
	public int mpCost;

	public abstract void OnUse(PlayerBehavior player);
}

public class GroundCrush : Skill {
	public GroundCrush() {
		name = "GroundCrush";
		coolDown = 5.0f;
		mpCost = 30;
	}

	public override void OnUse(PlayerBehavior player) { player.UseSkill(); }
}

public class Blessing : Skill {
	public Blessing() {
		name = "Blessing";
		coolDown = 5.0f;
		mpCost = 30;
	}

	public override void OnUse(PlayerBehavior player) { }
}

public class Heal : Skill {
	public Heal() {
		name = "Heal";
		coolDown = 5.0f;
		mpCost = 30;
	}

	public override void OnUse(PlayerBehavior player) {
		player.stats.HealHealth((int)((float)player.stats.maxHealth / 40.0f));
	}
}