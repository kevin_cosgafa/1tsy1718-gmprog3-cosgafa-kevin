﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour {
	public List<Item> inventory = new List<Item>();
	public int gold = 0;

	public List<Item> getItems() { return inventory; }
}
