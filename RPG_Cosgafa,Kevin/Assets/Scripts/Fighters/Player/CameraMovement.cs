﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {
	public GameObject target;

	private const float COMPENSATION = 1.0f;
	private const float LERP_SPEED = 0.02f;
	private const float RELATIVEX = -11.0f;
	private const float RELATIVEY = 13.0f;
	private const float RELATIVEZ = 7.0f;

	private Vector3 relativePosition;
	private Vector3 newRelativePosition;

	void Update() {
		if(target == null) { return; }

		relativePosition = new Vector3(
			this.gameObject.transform.position.x - RELATIVEX,
			this.gameObject.transform.position.y - RELATIVEY,
			this.gameObject.transform.position.z - RELATIVEZ);

		if(Vector3.Distance(target.transform.position, relativePosition) > COMPENSATION) {
			newRelativePosition = new Vector3(
				target.transform.position.x + RELATIVEX,
				target.transform.position.y + RELATIVEY,
				target.transform.position.z + RELATIVEZ);
			this.gameObject.transform.position = Vector3.Lerp(this.gameObject.transform.position, newRelativePosition, LERP_SPEED);
		}
	}
}
