﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCAnimations : MonoBehaviour {
	public Animator animatorController;

	public void Idle() { animatorController.SetInteger("animIndex", 0); }
	public void Talk() { animatorController.SetInteger("animIndex", 1); }

	void Start() { animatorController = this.GetComponent<Animator>(); }
}
