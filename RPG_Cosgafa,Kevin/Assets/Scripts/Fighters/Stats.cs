﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats {
	private const int BASE_HEALTH = 100;
	private const int BASE_ATTACKPOWER = 10;

	public int vitality { get; private set; }
	public int strength { get; private set; }
	public int maxHealth { get; private set; }
	public int attackPower { get; private set; }
	public int level { get; private set; }
	public int experience { get; private set; }
	public int experienceToLevel { get; private set; }
	public int statPoints { get; private set; }

	public int intelligence;
	public int spirit;
	public int magicalAttackPower;
	public int manaRegen;

	public List<Skill> skillList = new List<Skill>();

	private int _currentHealth;
	public int currentHealth {
		get { return _currentHealth; }
		private set {
			if(value > maxHealth) { _currentHealth = maxHealth; }
			else if(value < 0) { _currentHealth = 0; }
			else { _currentHealth = value; }
		}
	}

	public int maxMana;
	private int _currentMana;
	public int currentMana {
		get { return _currentMana; }
		private set {
			if(value > maxHealth) { _currentMana = maxMana; }
			else if(value < 0) { _currentMana = 0; }
			else { _currentMana = value; }
		}
	}

	public Stats() {
		vitality = 1;
		strength = 1;
		level = 1;
		experience = 0;
		RecalculateStats();

		skillList.Add(new GroundCrush());
	}

	public void HealHealth(int value) { currentHealth += value; }
	public void DamageHealth(int value) { currentHealth -= value; }

	public void SetVitality(int value) { vitality = value; RecalculateStats(); }
	public void SetStrength(int value) { strength = value; RecalculateStats(); }
	public void SetExperience(int value) { experience = value; RecalculateStats(); }

	public void AddExperience(int value) {
		experience += value;

		if(experience >= experienceToLevel) {
			statPoints++;
			level++;
			RecalculateStats();
			if(level == 2) { skillList.Add(new Heal()); }
			if(level == 3) { skillList.Add(new Blessing()); }
		}
	}

	public void IncrementVitality() {
		if(statPoints < 1) { Debug.Log("No stat points to spend."); return; }

		vitality++;
		statPoints--;
		RecalculateStats();
	}

	public void IncrementStrength() {
		if(statPoints < 1) { Debug.Log("No stat points to spend."); return; }

		strength++;
		statPoints--;
		RecalculateStats();
	}

	private void RecalculateStats() {
		maxHealth = BASE_HEALTH + ((vitality - 1) * 20);
		attackPower = BASE_ATTACKPOWER + ((strength - 1) * 10);
		currentHealth = maxHealth;
		experienceToLevel = Factorial(level) * 100;
		HealHealth(maxHealth);
	}

	private int Factorial(int value) {
		if(value < 0) { return 0; }
		if(value == 0 || value == 1) { return 1; }
		return value * Factorial(value - 1);
	}
}
