﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour {
	public GameObject location1;
	public GameObject location2;
	public GameObject location3;
	public GameObject location4;
	public GameObject location5;

	public GameObject goblinPrefab;
	public GameObject wildPigPrefab;
	public GameObject slimePrefab;

	public GameObject healthPotion;
	public GameObject goldStack;

	private const int MAX_MONSTER_INSTANCES = 10;
	private const float SPAWN_INTERVALS = 3.0f;

	private List<GameObject> locations = new List<GameObject>();
	private List<GameObject> monsterPrefabs = new List<GameObject>();
	private List<GameObject> dropList = new List<GameObject>();
	private GameObject reference;
	private bool isSpawning = false;
	private float timer = 0.0f;

	void Start() {
		monsterPrefabs.Add(goblinPrefab);
		monsterPrefabs.Add(wildPigPrefab);
		monsterPrefabs.Add(slimePrefab);

		locations.Add(location1);
		locations.Add(location2);
		locations.Add(location3);
		locations.Add(location4);
		locations.Add(location5);

		dropList.Add(healthPotion);
	}

	void Update() {
		if(GameObject.FindGameObjectsWithTag("Monster").Length < MAX_MONSTER_INSTANCES && !this.isSpawning) { isSpawning = true; }

		if(this.isSpawning) {
			timer +=Time.deltaTime;
			if(timer >= SPAWN_INTERVALS) {
				reference = Instantiate(monsterPrefabs[Random.Range(0, monsterPrefabs.Count)]);

				reference.tag = "Monster";
				reference.GetComponent<MonsterBehavior>().dropList = this.dropList;
				reference.GetComponent<MonsterBehavior>().goldDrop = this.goldStack;

				reference.transform.position = locations[Random.Range(0, locations.Count)].transform.position;
				reference.transform.position = new Vector3(
					Random.Range(reference.transform.position.x - 4.0f, reference.transform.position.x + 4.0f),
					reference.transform.position.y,
					Random.Range(reference.transform.position.z - 4.0f, reference.transform.position.z + 4.0f)
				);

				reference = null;

				timer = 0.0f;
				this.isSpawning = false;
			}
		}
	}
}
