﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class MonsterBehavior : Fighter {
	public int experienceValue;

	public GameObject goldDrop;
	public List<GameObject> dropList = new List<GameObject>();

	private Vector3 destination;
	private float idleTimer = 0.0f;
	private bool hasDropped = false;

	public void BeginChase(GameObject other) { idleTimer = 0.0f; target = other; }
	public void EndChase(GameObject other) {
		target = null;
		if(currentState != FSM_STATE.DEAD) { currentState = FSM_STATE.IDLE; }
	}

	protected override void UpdateIdleState() {
		if(target != null) { currentState = FSM_STATE.MOVE; }

		this.GetComponent<MonsterAnimations>().Idle();

		agent.speed = 1.5f;
		destination = agent.transform.position;
		idleTimer += Time.deltaTime;

		if(idleTimer >= Random.Range(3.0f, 5.0f)) {
			destination = new Vector3(
				Random.Range(this.transform.position.x - 5.0f, this.transform.position.x + 5.0f),
				0.0f,
				Random.Range(this.transform.position.z - 5.0f, this.transform.position.z + 5.0f));

			idleTimer = 0.0f;
			currentState = FSM_STATE.MOVE;
		}
	}

	protected override void UpdateMoveState() {
		if(target == null) {
			this.GetComponent<MonsterAnimations>().Move();

			agent.SetDestination(destination);
			if(Vector3.Distance(this.gameObject.transform.position, destination) < 2.0f)
				currentState = FSM_STATE.IDLE;

			if(agent.velocity.magnitude == 0.0f) {
				idleTimer += Time.deltaTime;
				if(idleTimer >= 1.5f) { idleTimer = 0.0f; currentState = FSM_STATE.IDLE; }
			}
		}

		if(target != null) {
			this.GetComponent<MonsterAnimations>().MoveArmed();

			agent.speed = 2.5f;

			destination = target.transform.position;
			agent.SetDestination(destination);

			if(Vector3.Distance(this.gameObject.transform.position, destination) < 2.0f) { currentState = FSM_STATE.ATTACK; }
		}
	}

	protected override void UpdateAttackState() {
		this.GetComponent<MonsterAnimations>().Attack();

		if(target == null || target.GetComponent<Fighter>().currentState == FSM_STATE.DEAD) {
			target = null;
			currentState = FSM_STATE.IDLE;
			destination = agent.transform.position;
			return;
		}

		this.gameObject.transform.LookAt(target.transform.position);

		if(Vector3.Distance(this.gameObject.transform.position, target.transform.position) > 2.0f) { currentState = FSM_STATE.MOVE; }
	}

	protected override void UpdateDeadState() {
		this.GetComponent<MonsterAnimations>().Dead();
		hpBar.enabled = false;

		if(this.hasDropped) { return; }

		GameObject reference;

		if(Random.Range(0.0f, 1.0f) >= 5.0f) {
			reference = Instantiate(goldDrop);
			reference.transform.position = this.transform.position;
			reference = null;
		}

		if(Random.Range(0.0f, 100.0f) <= 33.33f) {
			bool loop = true;
			while(loop) {
				foreach(GameObject item in dropList) {
					if(Random.Range(0, (float)dropList.Count) / (float)dropList.Count > 1.0f / (float)dropList.Count || dropList.Count == 1) {
						reference = Instantiate(item);
						reference.transform.position = this.transform.position;
						loop = false;
						break;
					}
				}
			}
		}

		hasDropped = true;
	}

	protected override void AdditionalStart() {
		hpBar = this.GetComponentInChildren<Image>();
		stats.SetExperience(experienceValue);
	}

	protected override void AdditionalUpdate() {
		this.GetComponentInChildren<Canvas>().transform.LookAt(Camera.main.transform);
	}
}
