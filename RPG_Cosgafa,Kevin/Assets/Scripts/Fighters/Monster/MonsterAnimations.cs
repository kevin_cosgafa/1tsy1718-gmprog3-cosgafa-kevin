﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAnimations : MonoBehaviour {
	public Animator animatorController;

	public void Idle() { animatorController.SetInteger("animIndex", 0); }
	public void Move() { animatorController.SetInteger("animIndex", 1); }
	public void MoveArmed() { animatorController.SetInteger("animIndex", 2); }
	public void Attack() { animatorController.SetInteger("animIndex", 3); }
	public void Hurt() { animatorController.SetInteger("animIndex", 4); }
	public void Skill() { animatorController.SetInteger("animIndex", 5); }
	public void Dead() { animatorController.SetInteger("animIndex", 6); }

	void Start() { animatorController = this.GetComponent<Animator>(); }
}
