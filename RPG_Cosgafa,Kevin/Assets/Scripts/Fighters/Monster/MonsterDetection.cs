﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterDetection : MonoBehaviour {
	void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag == "Player" && this.GetComponentInParent<MonsterBehavior>().currentState != Fighter.FSM_STATE.ATTACK &&
			other.gameObject.GetComponent<PlayerBehavior>().currentState != Fighter.FSM_STATE.DEAD) {

			this.GetComponentInParent<MonsterBehavior>().BeginChase(other.gameObject);
		}
	}

	void OnTriggerStay(Collider other) {
		if(other.gameObject.tag == "Player" && other.gameObject.GetComponent<PlayerBehavior>().currentState == Fighter.FSM_STATE.DEAD) {
			this.GetComponentInParent<MonsterBehavior>().EndChase(other.gameObject);
		}
	}

	void OnTriggerExit(Collider other) {
		if(other.gameObject.tag == "Player") { this.GetComponentInParent<MonsterBehavior>().EndChase(other.gameObject); }
	}
}
