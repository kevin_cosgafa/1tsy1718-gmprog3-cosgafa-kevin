﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemButton : MonoBehaviour, IPointerDownHandler {
	public Item item;

	#region IPointerDownHandler implementation

	public void OnPointerDown (PointerEventData eventData)
	{
		if(item != null) { item.Use(this.gameObject.GetComponentInParent<InventoryManager>().player); }
	}

	#endregion


}
