﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//[System.Serializable]
public class ItemData {
	public Sprite image;

	public bool isUsed;
	public string name;
	public string description;
	public int value;

	public ItemData() { isUsed = false; }
}

public abstract class Item {
	public ItemData itemData = new ItemData();

	public virtual void Use(GameObject player) { Debug.Log("Used"); Debug.Log(itemData.image); itemData.isUsed = true; }
}

public class HealthPotion : Item {
	public HealthPotion() {
		itemData.name = "Health Potion";
		itemData.description = "Recover 50HP";
	}

	public override void Use(GameObject player) {
		base.Use(player);

		player.GetComponent<PlayerBehavior>().stats.HealHealth(50);
	}
}

public class Gold : Item {
	public Gold() {
		itemData.name = "Gold";
		itemData.value = (int)Random.Range(100.0f, 1000.0f);
	}
	public override void Use(GameObject player) {
		base.Use(player);
		player.GetComponent<PlayerInventory>().gold += this.itemData.value;
	}
}