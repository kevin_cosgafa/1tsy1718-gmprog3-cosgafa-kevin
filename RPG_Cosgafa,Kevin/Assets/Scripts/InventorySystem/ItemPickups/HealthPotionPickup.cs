﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPotionPickup : ItemPickup {
	void Start() {
		this.item = new HealthPotion();
	}
}