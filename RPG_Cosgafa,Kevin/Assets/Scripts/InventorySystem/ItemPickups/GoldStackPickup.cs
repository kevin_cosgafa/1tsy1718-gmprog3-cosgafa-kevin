﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldStackPickup : ItemPickup {
	void Start() {
		this.item = new Gold();
	}
}
