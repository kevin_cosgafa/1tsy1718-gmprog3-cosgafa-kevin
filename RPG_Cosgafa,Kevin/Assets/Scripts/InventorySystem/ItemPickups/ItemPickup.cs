﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemPickup : MonoBehaviour {
	public Sprite menuImage;

	public Item item;

	void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag != "Player") { return; }

		if(this.item.itemData.name == "Gold") { item.Use(other.gameObject); }
		if(this.item.itemData.name != "Gold") {
			item.itemData.image = menuImage;
			other.gameObject.GetComponent<PlayerInventory>().inventory.Add(item);
		}

		Destroy(this.gameObject);
	}
}