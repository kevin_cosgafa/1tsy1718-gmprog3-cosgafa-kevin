﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour {
	public GameObject player;

	private List<Item> inventory = new List<Item>();
	private List<Button> buttons = new List<Button>();

	void Start() {
		foreach(Button button in this.gameObject.GetComponentsInChildren<Button>()) {
			buttons.Add(button);
			button.gameObject.SetActive(false);
		}
	}

	void Update() {
		this.inventory = player.GetComponent<PlayerInventory>().getItems();
		this.gameObject.GetComponentInChildren<Text>().text = "$" + player.GetComponent<PlayerInventory>().gold;

		foreach(Item item in inventory) {
			foreach(Button button in buttons) {
				if(button.GetComponent<ItemButton>().item == item) { break; }

				if(!button.gameObject.activeSelf && button.GetComponent<ItemButton>().item != item) {
					button.GetComponent<ItemButton>().item = item;
					button.image.sprite = item.itemData.image;
					button.gameObject.SetActive(true);
					break;
				}
			}
		}

		foreach(Button button in this.gameObject.GetComponentsInChildren<Button>()) {
			if(button.GetComponent<ItemButton>().item != null && button.GetComponent<ItemButton>().item.itemData.isUsed) {
				player.GetComponent<PlayerInventory>().inventory.Remove(button.GetComponent<ItemButton>().item);
				button.GetComponent<ItemButton>().item = null;
				button.gameObject.SetActive(false);
			}
		}
	}
}
